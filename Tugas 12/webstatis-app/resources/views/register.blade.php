<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
      <label for="fname">First name:</label><br /><br />
      <input type="text" name="fname" id="fname" /><br /><br />
      <label for="lname">Last name:</label><br /><br />
      <input type="text" name="lname" id="lname" /><br /><br />
      <label for="gender">Gender</label><br /><br />
      <input type="radio" name="gender" id="gender" value="1" /> Male<br />
      <input type="radio" name="gender" id="gender" value="2" /> Female<br />
      <input type="radio" name="gender" id="gender" value="3" />
      Other<br /><br />
      <label for="nationality">Nationality</label><br /><br />
      <select name="nationality" id="nationality">
        <option value="Indonesian" value="1">Indonesian</option>
        <option value="Japanese" value="2">Japanese</option>
        <option value="United States" value="3">United States</option></select
      ><br /><br />
      <label for="lang">Language Spoken:</label><br /><br />
      <input type="checkbox" name="lang" id="lang" value="1" />
      Bahasa Indonesia
      <br />
      <input type="checkbox" name="lang" id="lang" value="2" />
      Japanese<br />
      <input type="checkbox" name="lang" id="lang" value="3" />
      English <br /><br />
      <label for="bio">Bio:</label><br /><br />
      <textarea name="bio" id="" cols="30" rows="10"></textarea><br />
      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
